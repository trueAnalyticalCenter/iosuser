//
//  Keyword.swift
//  AnyStat
//
//  Created by Yury Bogdanov on 09.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import RealmSwift

class Keyword: Object {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var personID = 0

}
