//
//  Page.swift
//  AnyStat
//
//  Created by Yury Bogdanov on 09.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import RealmSwift

class Page: Object {
    
    dynamic var ID = 0
    dynamic var URL = ""
    dynamic var siteID = 0
    dynamic var foundDateTime = Date()    // Нужны ли эти даты в приложении для пользователя?
    dynamic var lastScanDate = Date()
}
