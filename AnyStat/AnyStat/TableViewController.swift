//
//  TableViewController.swift
//  AnyStat
//
//  Created by Alexey Efimov on 06.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import UIKit
import Realm

class TableViewController: UITableViewController {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch segmentControl.selectedSegmentIndex {
        case 0:
            let data = uiRealm.objects(Site.self)
            return data.count
        case 1:
            let data = uiRealm.objects(Person.self)
            return data.count
        default:
            return 0
        }
    }
    
    @IBAction func refresh(_ sender: AnyObject) {
        netWork.update()
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        switch segmentControl.selectedSegmentIndex {
        case 0:
            let data = uiRealm.objects(Site.self)
            cell.textLabel?.text = "\(data[indexPath.row].name)"
        case 1:
            let data = uiRealm.objects(Person.self)
            cell.textLabel?.text = "\(data[indexPath.row].name)"
        default:
            break
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (segmentControl.selectedSegmentIndex == 0) {
            return "Сайты"
        }
        else {
            return "Личности"
        }
    }
    
    // MARK: Data Source Methods
    @IBAction func dataSourceChanged(_ sender: AnyObject) {
        tableView.reloadData()
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetails" {
            let destination = segue.destination as! DetailTableViewController
            let indexPath = self.tableView.indexPathForSelectedRow!
            switch segmentControl.selectedSegmentIndex {
            case 0:
                destination.obj = .person
                let nameOfSite = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!
                let data = uiRealm.objects(Site.self)
                for i in 0..<data.count {
                    if data[i].name == nameOfSite {
                        destination.objId = data[i].id
                    }
                }
            case 1:
                destination.obj = .site
                let nameOfPerson = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!
                let data = uiRealm.objects(Person.self)
                for i in 0..<data.count {
                    if data[i].name == nameOfPerson {
                        destination.objId = data[i].id
                    }
                }
                
            default:
                break
            }
        }
    }
    


}
