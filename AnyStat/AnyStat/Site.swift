//
//  Site.swift
//  AnyStat
//
//  Created by Yury Bogdanov on 09.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import RealmSwift

class Site: Object {
    
    dynamic var id = 0
    dynamic var name = ""
    //var pages = List<Page>()
    override static func primaryKey() -> String? {
        return "id"
    }
}
