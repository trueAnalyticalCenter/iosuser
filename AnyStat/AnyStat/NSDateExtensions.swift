//
//  NSDateExtensions.swift
//  AnyStat
//
//  Created by Alexey Efimov on 06.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation

extension Date {
    struct Formatter {
        static let longDate = DateFormatter(dateStyle: .long)
    }
    var longDate: String {
        return Formatter.longDate.string(from: self as Date)
    }
}
