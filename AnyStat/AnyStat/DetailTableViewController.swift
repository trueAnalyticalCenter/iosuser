//
//  DetailTableViewController.swift
//  AnyStat
//
//  Created by Alexey Efimov on 06.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import UIKit
enum typeOfDetailTable {
    case site
    case person
}

class DetailTableViewController: UITableViewController {
    
    var obj: typeOfDetailTable = .site
    var objId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch obj {
        case .site:
            let data = uiRealm.objects(Site.self)
            return data.count
        case .person:
            let data = uiRealm.objects(Person.self)
            return data.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath)
        let personPageRank = uiRealm.objects(PersonPageRank.self)
        
        switch obj {
        case .site:
            let sites = uiRealm.objects(Site.self)
            cell.textLabel?.text = "\(sites[indexPath.row].name)"
            cell.detailTextLabel?.text = "0"
            for i in 0..<personPageRank.count {
                if (personPageRank[i].personID == objId) && (personPageRank[i].siteID == sites[indexPath.row].id) {
                    cell.detailTextLabel?.text = "\(personPageRank[i].rank)"
                    
                }
            }
        case .person:
            let persons = uiRealm.objects(Person.self)
            cell.textLabel?.text = "\(persons[indexPath.row].name)"
            cell.detailTextLabel?.text = "0"
            for i in 0..<personPageRank.count {
                if (personPageRank[i].siteID == objId) && (personPageRank[i].personID == persons[indexPath.row].id) {
                    cell.detailTextLabel?.text = "\(personPageRank[i].rank)"
                    
                }
            }
        }
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch obj {
        case .site:
            return uiRealm.objects(Person.self).filter("id = \(objId)").first?.name
        case .person:
            return uiRealm.objects(Site.self).filter("id = \(objId)").first?.name
        }
    }

}
