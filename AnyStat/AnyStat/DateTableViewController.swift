//
//  DateTableViewController.swift
//  AnyStat
//
//  Created by Alexey Efimov on 06.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import UIKit

class DateTableViewController: UITableViewController {
    
    var dateFormatter = DateFormatter()
    var events = [Event]()
    var datePickerIndexPath: IndexPath?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFormatter()
        createEvents()

        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]

    }

    func setFormatter() {
        dateFormatter.dateStyle = .long
    }
    
    func createEvents() {
        let event1 = Event(title: "Дата с:", time: dateFormatter.date(from: Date().longDate)!)
        let event2 = Event(title: "Дата по:", time: dateFormatter.date(from: Date().longDate)!)
        
        events.append(event1)
        events.append(event2)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = events.count
        if datePickerIndexPath != nil {
            rows += 1
        }
        return rows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if datePickerIndexPath != nil && datePickerIndexPath!.row == indexPath.row {
            cell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell")!
            let datePicker = cell.viewWithTag(1) as! UIDatePicker
            let event = events[indexPath.row - 1]
            datePicker.setDate(event.time, animated: true)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "DateCell")!
            let event = events[indexPath.row]
            cell.textLabel!.text = event.title
            cell.detailTextLabel!.text = dateFormatter.string(from: event.time)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        if datePickerIndexPath != nil && (datePickerIndexPath?.row)! - 1 == indexPath.row {
            tableView.deleteRows(at: [datePickerIndexPath! as IndexPath], with: .fade)
            datePickerIndexPath = nil
        } else {
            if datePickerIndexPath != nil {
                tableView.deleteRows(at: [datePickerIndexPath! as IndexPath], with: .fade)
            }
            datePickerIndexPath = calculateDatePickerIndexPath((indexPath as NSIndexPath) as IndexPath)
            tableView.insertRows(at: [datePickerIndexPath! as IndexPath], with: .fade)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.endUpdates()
    }
    
    func calculateDatePickerIndexPath(_ indexPathSelected: IndexPath) -> IndexPath {
        if datePickerIndexPath != nil && ((datePickerIndexPath as NSIndexPath?)?.row)! < (indexPathSelected as NSIndexPath).row {
            return IndexPath(row: indexPathSelected.row, section: 0)
        } else {
            return IndexPath(row: (indexPathSelected as NSIndexPath).row + 1, section: 0)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight = tableView.rowHeight
        if datePickerIndexPath != nil && datePickerIndexPath!.row == indexPath.row {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell")!
            rowHeight = cell.frame.height
        }
        return rowHeight
    }
    
    @IBAction func changeDate(_ sender: UIDatePicker) {
        let parentIndexPath = IndexPath(row: ((datePickerIndexPath as NSIndexPath?)?.row)! - 1, section: 0)
        let event = events[parentIndexPath.row]
        event.time = sender.date
        let eventCell = tableView.cellForRow(at: parentIndexPath as IndexPath)!
        eventCell.detailTextLabel!.text = dateFormatter.string(from: sender.date)
    }

}
