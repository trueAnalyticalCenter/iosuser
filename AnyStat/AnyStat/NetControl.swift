//
//  NetControl.swift
//  AnyStat
//
//  Created by Vitaly on 23.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum typeOfData {
    case Sites
    case Pages
    case Persons
    case Ranks
}

typealias JSONStandard = [String : AnyObject]

class NetControl {
    let Url = "http://52.37.48.231:8080"
    
    var sites = [String]()
    var pages = [String]()
    var persons = [String]()
    var ranks = [String]()
    var updateFlag = false
    
    func update() {
        updateFlag = true
        callAlamoSites(url: Url)
        callAlamoPersons(url: Url)
        callAlamoRank(url: Url)
    }
    
    func callAlamoSites(url: String) {
        Alamofire.request(url + "/sites/page/" + "0").responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                for i in 0..<json["content"].count {
                    let taskSite = Site(value: ["name":json["content"][i]["name"].string!, "id": json["content"][i]["id"].int!])
                    try! uiRealm.write {
                        uiRealm.add(taskSite, update: true)
                    }
                }
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func callAlamoPersons(url: String) {
        Alamofire.request(url + "/persons/page/" + "0").responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                for i in 0..<json["content"].count {
                    let taskSite = Person(value: ["name":json["content"][i]["name"].string!, "id": json["content"][i]["id"].int!])
                    try! uiRealm.write {
                        uiRealm.add(taskSite, update: true)
                    }
                }
            case .failure(let error):
                print(error)
            }
            self.updateFlag = false
        })
    }
    
    func callAlamoRank(url: String) {
        let rankUrl = "/ranks/total/person/"
        let rankUrlSite = "/site/"
        let dataPerson = uiRealm.objects(Person.self)
        let dataSite = uiRealm.objects(Site.self)
        
        for i in 0..<dataPerson.count {
            let personId = dataPerson[i].id
            for j in 0..<dataSite.count {
                let siteId = dataSite[j].id
                Alamofire.request(url + rankUrl + "\(personId)" + rankUrlSite + "\(siteId)").responseJSON(completionHandler: {
                    response in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        if let rank = json["rank"].int {
                            let personPageRank = PersonPageRank(value: ["personID": personId, "siteID": siteId, "rank": rank])
                            try! uiRealm.write {
                                uiRealm.add(personPageRank)
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                })
                
            }
        }
    }

    

    
}
