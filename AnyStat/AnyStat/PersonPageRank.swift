//
//  PersonPageRank.swift
//  AnyStat
//
//  Created by Yury Bogdanov on 09.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import RealmSwift

class PersonPageRank: Object {
    
    dynamic var personID = 0
    dynamic var siteID = 0
    dynamic var rank = 0
}
