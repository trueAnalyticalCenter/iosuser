//
//  LoginViewController.swift
//  AnyStat
//
//  Created by Alexey Efimov on 12.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButtonPressed(_ sender: AnyObject) {
        
        PFUser.logInWithUsername(inBackground: loginTextField.text!, password: passwordTextField.text!) { (user: PFUser?, error: Error?) in
            guard error == nil else {
                print("Can't login")
                print("error: \(error?.localizedDescription)")
                return
            }
            netWork.update()
            self.performSegue(withIdentifier: "Login", sender: self)
        }
    }

}
