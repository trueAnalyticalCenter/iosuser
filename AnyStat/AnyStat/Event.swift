//
//  Event.swift
//  AnyStat
//
//  Created by Alexey Efimov on 06.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation

class Event {
    let title: String
    var time: Date
    init(title: String, time: Date) {
        self.title = title
        self.time = time
    }
}
