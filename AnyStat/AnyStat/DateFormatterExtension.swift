//
//  DateFormatterExtension.swift
//  AnyStat
//
//  Created by Alexey Efimov on 06.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation

extension DateFormatter {
    convenience init(dateStyle: DateFormatter.Style) {
        self.init()
        self.dateStyle = dateStyle
    }
}
