//
//  Person.swift
//  AnyStat
//
//  Created by Yury Bogdanov on 09.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import RealmSwift

class Person: Object {
    
    dynamic var id = 0
    dynamic var name = ""
    //var keywords = List<Keyword>()
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
