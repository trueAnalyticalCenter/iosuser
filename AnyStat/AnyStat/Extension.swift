//
//  Extension.swift
//  AnyStat
//
//  Created by Vitaly on 10.10.16.
//  Copyright © 2016 True Analytical Cente. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let brand = UIColor(red: 228/255, green: 86/255, blue: 85/255, alpha: 1.0)
    static let dark = UIColor(red: 159/255, green: 50/255, blue: 47/255, alpha: 1.0)
    static let blackOpacity = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
}

extension UITableViewCell {
    func prepareDisclosureIndicator() {
        for case let button as UIButton in subviews {
            let image = button.backgroundImage(for: .normal)?.withRenderingMode(.alwaysTemplate)
            button.setBackgroundImage(image, for: .normal)
        }
    }
    
    func customize() {
        textLabel?.textColor = Colors.dark
        detailTextLabel?.textColor = Colors.blackOpacity
    }
}
